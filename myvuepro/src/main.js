import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import Main from './components/Main.vue'
import User from './components/User.vue'
import Detail from './components/detail.vue'
import Header from './components/Header.vue'
import Detail_teacher from './components/detail-teacher.vue'
import Js from './components/js.vue'
import Vuejs from './components/vue.vue'
import register from './components/register.vue'
import Login from './components/login.vue'
import Logined from './components/logined.vue'
import My from './components/my.vue'
import logout from './components/logout.vue'
import Buy from './components/Buy.vue'
import Liuyanban from './components/liuyanban.vue'
import Shopping from './components/Shopping.vue'
import Mintui from './components/mintui.vue'


import Mint from 'mint-ui';
Vue.use(Mint);
import 'mint-ui/lib/style.css';



import axios from 'axios'
Vue.prototype.$axios = axios
//注册了一个全局过滤器
Vue.filter('ymd', function(value) {
	return value.slice(0, 10);
})

Vue.filter('esclips', function(value) {
	return value.slice(0, 220) + '......';
})
Vue.filter('tabtext', function(value) {
	if (value == 'share') {
		return '分享';
	}
	if (value == 'ask') {
		return '问答';
	}
	if (value == 'good') {
		return '精华';
	}
	if (value == 'job') {
		return '招牌';
	}
})
// Vue.directive('rainbow', {
// 	bind(el, binding, vnode) {
// 		el.style.color = "#" + Math.random().toString(16).slice(2, 8);
// 	}
// })

Vue.use(VueRouter) /* 使用对象 */
Vue.config.productionTip = false
//配置你的路由规则
var router = new VueRouter({
	mode: 'history',
	routes: [{
			name: 'main',
			path: '/',
			components: {
				/* header  name的名字 */
				/* Header 组件模板名字*/
				header: Header,
				default: Main,
			
			}
		},
		{
			name: 'teacher',
			path: '/user',
			components: {
				header: Header,
				default: User,
			
			}
		},
		{
			name: 'detail',
			path: '/detail',
			components: {
				header: Header,
				default: Detail,
			
			}
		},
		{
			name: 'detail-teacher',
			path: '/detail-teacher',
			components: {
				header: Header,
				default: Detail_teacher,
			
			}
		},
		{
			name: 'course',
			path: '/course',
			components: {
				header: Header,
			},
			/* 二级路由 */
			children: [{
					name: 'course_js',
					path: 'js',
					component: Js
				},
				{
					name: 'course_Vue',
					path: 'vue',
					component: Vuejs
				},
			]
		},

	{
			name: 'register',
			path: '/register',
			components: {
				header: Header,
				default:register,
				
			}
		},
		{
			name: 'login',
			path: '/login',
			components: {
				header: Header,
				default: Login,
				
			}
		},
		{
			name: 'logined',
			path: '/logined',
			components: {
				header: Header,
				default: Logined,
				
			},
			// 			meta: {
			// 				login_required: true
			// 			}
		},
		{
			name: 'logout',
			path: '/logout',
			components: {
				header: Header,
				default: logout,
				
			}
		},
		{
			name: 'my',
			path: '/my',
			components: {
				header: Header,
				default: My,
				
			},

			/* 路由拦截 */
			meta: {
				login_required: true
			}
		},
		{
			name: 'buy',
			path: '/buy',
			components: {
				header: Header,
				default: Buy,
				
			}
		},
		{
			name: 'shopping', //名字
			path: '/Shopping', //路径
			components: {
				header: Header,
				default: Shopping,
				
			},
			meta: {
				login_required: true //路由拦截
			}
		},
		{
			name: 'message',
			path: '/message',
			components: {
				header: Header,
				default: Liuyanban,
				
			}
	
			},
		{
			name: 'mintui',
			path: '/mintui',
			components: {
				header: Header,
				default: Mintui,
				
			}
		},
	]
});
router.beforeEach(function(to, from, next) {
	var logged_in = false;
	logged_in = window.localStorage.getItem("logged"); //从本地存储获取值
	//var logged_in = true;
	if (!logged_in && to.path == '/my') {
		next('/login')
	} else {
		next()
	}
})
new Vue({
	router, //简写成router
	render: h => h(App),
}).$mount('#app')
