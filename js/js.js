//17110100517 Zhu Jiacheng
//2019-12-09
//高级网页设计
var swiper = new Swiper('.islide1', {
    loop: true,
    slidesPerView: 'auto',
    loopedSlides: 5,
       autoplay: {
         delay: 2500,
         disableOnInteraction: false,
       },
     pagination: {
       el: '.swiper-pagination',
       clickable: true,
     },
   });
 
   var islide = new Swiper('.islide2', {
     loop: true,
     slidesPerView: 1,
     spaceBetween: 10,
     freeMode: true,
 });
 
 
  window.onscroll = function() {
       scrollFunction()
   };
    
  function scrollFunction() {
       console.log(121);
       if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
           document.getElementById("btnTop").style.display = "block";
      } else {
          document.getElementById("btnTop").style.display = "none";
      }
     }
  function returnTop() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
  }


  $(document).ready(function () {
    //左侧菜单
    $(".menuList li").each(function () {
        $(this).click(function () {
            var title = $(this).text();
            $(".menuList li").removeClass("active");
            $(this).addClass("active");
        });
    });

    //监听菜单和产品滚动事件
    $('.proList').scroll(function () {
        Load();
    });
   // LunBo();
});

function Load() {

    var scrollTopHeight = $('.proList').scrollTop();
    
    if (scrollTopHeight > 0) {
        // div 向下滚动了
        $(".banner").fadeOut(500);

        //重新设定菜单和产品高度
        $(".menuList").css("height", "605px");
        $(".proList").css("height", "560px");

    } else if (scrollTopHeight ==0) {

        // 滚动到头部了
        $(".banner").fadeIn(500);

        //重新设定菜单和产品高度
        $(".menuList").css("height", "465px");
        $(".proList").css("height", "420px");
    }
}

//banner轮播
function LunBo() {
    var slideA = slide($(".side"), 3000);
    setTimeout(slideA, 2000);
    function slide(Mobj, s) {//幻灯片
        var n = 0;
        var obj = Mobj.find(".m").children("li");
        var select_obj;
        var leng = obj.length - 1;
        var intA;
        var curClass = "cur";

        childrenLi()
        obj.css({ display: "none" });
        obj.eq(n).fadeIn("slow");
        select_obj.eq(n).addClass(curClass);
        select_obj.mouseover(function () {
            var i = n;
            //alert(n);
            n = $(this).index();
            select_obj.eq(i).removeClass(curClass);
            select_obj.eq(n).addClass(curClass);
            obj.eq(i).fadeOut("slow");
            obj.eq(n).fadeIn("slow");
        });
        function childrenLi() {
            var str = '<li class="cur">1</li>';
            for (i = 2; i <= Mobj.find(".m").children("li").length; i++) {
                str += '<li>' + i + '</li>'
            }
            Mobj.find(".s").html(str);
            select_obj = Mobj.find(".s").children("li");

        }
        function slide_main() {
            if (n < leng) {
                select_obj.eq(n).removeClass(curClass);
                obj.eq(n).fadeOut("slow");
                n = n + 1;
                select_obj.eq(n).addClass(curClass);
                obj.eq(n).fadeIn("slow");
            }
            else {
                select_obj.eq(n).removeClass(curClass);
                obj.eq(n).fadeOut("slow");
                n = 0;
                select_obj.eq(n).addClass(curClass);
                obj.eq(n).fadeIn("slow");
            }
        }
        function setI() { clearInterval(intA); intA = setInterval(slide_main, s) }
        Mobj.hover(function () { clearInterval(intA); }, function () { clearInterval(intA); intA = setInterval(slide_main, s); })
        return setI;
    };
}













